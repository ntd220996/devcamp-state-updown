import { Component } from "react";

class Count extends Component {
    constructor(props) {
        super(props);

        this.state = {
            count: 0
        }
    }

    onIncreaseClick = () => {

        console.log('Tăng lên')
        this.setState({
            count: this.state.count + 1
        })
    }
    
    onReducedClick = () => {
        console.log('Giảm đi')
        this.setState({
            count: this.state.count - 1
        })
    }

    render() {
        return (
            <div>
                <button onClick={this.onIncreaseClick} style={{marginBottom: '10px'}}  > Increase </button>
                <br/>
                <button style={{marginBottom: '10px'}} onClick = {this.onReducedClick} > Reduced </button>
                <br/>
                <span> Count: {this.state.count} </span>
            </div>
        )
    }
}

export default Count